//
//  TWTweetModel.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWBaseModel.h"

@interface TWTweetModel : TWBaseModel

@property (nonatomic, copy, readonly) NSNumber *ID;
@property (nonatomic, copy, readonly) NSString *accountName;
@property (nonatomic, copy, readonly) NSString *smallPhotoUrl;
@property (nonatomic, copy, readonly) NSString *largePhotoUrl;
@property (nonatomic, copy, readonly) NSString *profileImageUrl;

- (TWTweetModel *)initWithDictionary:(NSDictionary *)dic;

@end
