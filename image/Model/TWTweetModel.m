//
//  TWTweetModel.m
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWTweetModel.h"

@interface TWTweetModel()

@property (nonatomic, copy, readwrite) NSNumber *ID;
@property (nonatomic, copy, readwrite) NSString *accountName;
@property (nonatomic, copy, readwrite) NSString *smallPhotoUrl;
@property (nonatomic, copy, readwrite) NSString *largePhotoUrl;
@property (nonatomic, copy, readwrite) NSString *profileImageUrl;

@end

@implementation TWTweetModel

- (TWTweetModel *)initWithDictionary:(NSDictionary *)dic {
    NSDictionary *entities = dic[@"entities"];
    if (![entities isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    NSArray *medies = entities[@"media"];
    if (![medies isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSDictionary *media = medies.firstObject;
    if (![media isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    NSDictionary *user = dic[@"user"];
    if (![user isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    NSString *photoUrl = media[@"media_url_https"];
    if (photoUrl.length == 0) {
        photoUrl = media[@"media_url"];
    }
    if (photoUrl.length == 0) {
        return nil;
    }
    
    NSString *smallPhotoUrl = photoUrl;
    NSString *largePhotoUrl = photoUrl;
    NSDictionary *sizes = media[@"sizes"];
    if (sizes[@"small"]) {
        smallPhotoUrl = [smallPhotoUrl stringByAppendingString:@":small"];
    }
    if (sizes[@"large"]) {
        largePhotoUrl = [largePhotoUrl stringByAppendingString:@":large"];
    }
    
    NSString *profileImageUrl = user[@"profile_image_url_https"];
    if (profileImageUrl.length == 0) {
        profileImageUrl = user[@"profile_image_url"];
    }
    TWTweetModel *model = [[TWTweetModel alloc] init];
    model.ID = dic[@"id"];
    model.accountName = user[@"name"];
    model.smallPhotoUrl = smallPhotoUrl;
    model.largePhotoUrl = largePhotoUrl;
    model.profileImageUrl = profileImageUrl;
    return model;
}
@end
