//
//  TWFullScreenView.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TWTweetModel;

@interface TWFullScreenView : UIView

- (instancetype)initWithFrame:(CGRect)frame
                        model:(TWTweetModel *)model;

@end
