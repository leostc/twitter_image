//
//  TWFullScreenView.m
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWFullScreenView.h"
#import "TWTweetModel.h"
#import "UIImageView+WebCache.h"
#import "SDImageCache.h"

static CGFloat const FULL_SCREEN_ANIMATION_TIME =  0.5;
static NSInteger const ACCOUNT_NAME_LABEL_HEIGHT = 40;
static NSInteger const PROFILE_IMAGE_HEIGHT_WIDTH = 40;
static NSInteger const SPACE_BETWEEN_PROFILE_IMAGE_AND_ACCOUNT_NAME = 10;
static NSInteger const IPHONE_STATUS_BAR_HEIGHT = 20;

@interface TWFullScreenView ()

@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIControl *backgroudView;
@property (strong, nonatomic) UILabel *accountNameLabel;
@property (strong, nonatomic) UIImageView *profileImageView;

@end

@implementation TWFullScreenView

- (instancetype)initWithFrame:(CGRect)frame model:(TWTweetModel *)model {
    if (self = [super initWithFrame:frame]) {
        [self setupViewWithModel:model];
    }
    return self;
}

- (void)setupViewWithModel:(TWTweetModel *)model {
    self.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.backgroudView];
    
    [self addSubview:self.imageView];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.largePhotoUrl]
                      placeholderImage:[[SDImageCache sharedImageCache] imageFromCacheForKey:model.smallPhotoUrl]];
  
    self.accountNameLabel.text = model.accountName;
    [self addSubview:self.accountNameLabel];
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:model.profileImageUrl]];
    [self addSubview:self.profileImageView];
    
    [self openImageViewWithAnimation];
}

- (void)openImageViewWithAnimation {
    [UIView animateWithDuration:FULL_SCREEN_ANIMATION_TIME animations:^{
        self.backgroudView.alpha = 1;
        self.profileImageView.alpha = 1;
        self.accountNameLabel.alpha = 1;
        [self.imageView setFrame:CGRectMake(0, ACCOUNT_NAME_LABEL_HEIGHT + IPHONE_STATUS_BAR_HEIGHT, self.frame.size.width, self.frame.size.height - ACCOUNT_NAME_LABEL_HEIGHT - IPHONE_STATUS_BAR_HEIGHT)];
    }];
}

- (void)closeView {
    [UIView animateWithDuration:FULL_SCREEN_ANIMATION_TIME animations:^{
        self.backgroudView.alpha = 0;
        self.profileImageView.alpha = 0;
        self.accountNameLabel.alpha = 0;
        [self.imageView setFrame:CGRectZero];
        self.imageView.center = self.center;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - getter

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _imageView.center = self.center;
        [_imageView setContentMode:UIViewContentModeScaleAspectFit];
    }
    return _imageView;
}

- (UIControl *)backgroudView {
    if (!_backgroudView) {
        _backgroudView = [[UIControl alloc] initWithFrame:self.frame];
        _backgroudView.backgroundColor = [UIColor blackColor];
        _backgroudView.alpha = 0;
        [_backgroudView addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchDown];
    }
    return _backgroudView;
}

- (UIImageView *)profileImageView {
    if (!_profileImageView) {
        CGRect rect = CGRectMake(SPACE_BETWEEN_PROFILE_IMAGE_AND_ACCOUNT_NAME,
                                 IPHONE_STATUS_BAR_HEIGHT,
                                 PROFILE_IMAGE_HEIGHT_WIDTH,
                                 PROFILE_IMAGE_HEIGHT_WIDTH);
        _profileImageView = [[UIImageView alloc] initWithFrame:rect];
        [_profileImageView setContentMode:UIViewContentModeScaleAspectFit];
        _profileImageView.alpha = 0;
    }
    return _profileImageView;
}

- (UILabel *)accountNameLabel {
    if (!_accountNameLabel) {
        CGRect rect = CGRectMake(PROFILE_IMAGE_HEIGHT_WIDTH + 2 * SPACE_BETWEEN_PROFILE_IMAGE_AND_ACCOUNT_NAME,
                                 IPHONE_STATUS_BAR_HEIGHT,
                                 self.frame.size.width - PROFILE_IMAGE_HEIGHT_WIDTH - 2 * SPACE_BETWEEN_PROFILE_IMAGE_AND_ACCOUNT_NAME,
                                 ACCOUNT_NAME_LABEL_HEIGHT);
        _accountNameLabel = [[UILabel alloc] initWithFrame:rect];
        _accountNameLabel.textColor = [UIColor whiteColor];
        _accountNameLabel.alpha = 0;
    }
    return _accountNameLabel;
}


@end
