//
//  TWGridImageCollectionViewCell.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TWTweetModel;

@interface TWGridImageCollectionViewCell : UICollectionViewCell

- (void)loadViewWithModel:(TWTweetModel *)model;

@end
