//
//  TWGridImageCollectionViewCell.m
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWGridImageCollectionViewCell.h"
#import "TWTweetModel.h"
#import "UIImageView+WebCache.h"

@interface TWGridImageCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation TWGridImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)loadViewWithModel:(TWTweetModel *)model {
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.smallPhotoUrl]];
}

@end
