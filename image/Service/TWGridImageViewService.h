//
//  TWGridImageViewService.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TWTweetModel;

typedef void(^CVDataRequestSuccessBlock)(NSArray<TWTweetModel *> *models);
typedef void(^CVDataRequestFailureBlock)(NSError *error);

@interface TWGridImageViewService : NSObject

- (void)fetchFistPageDataWithQuery:(NSString *)query
                           success:(CVDataRequestSuccessBlock)success
                           failure:(CVDataRequestFailureBlock)failure;

- (void)fetchMorePageDataWithQuery:(NSString *)query
                             maxId:(NSNumber *)ID
                           success:(CVDataRequestSuccessBlock)success
                           failure:(CVDataRequestFailureBlock)failure;

@end
