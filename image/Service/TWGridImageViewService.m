//
//  TWGridImageViewService.m
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWGridImageViewService.h"
#import "TWTweetModel.h"
#import "AFNetworking.h"
#import "TWConfig.h"
#import "TWStringUtil.h"

static NSInteger const PAGE_COUNT = 40;

@interface TWGridImageViewService()

@property (copy, nonatomic) NSString *accessToken;

@end

@implementation TWGridImageViewService

- (void)fetchFistPageDataWithQuery:(NSString *)query
                           success:(CVDataRequestSuccessBlock)success
                           failure:(CVDataRequestFailureBlock)failure {
    __weak __typeof(self) weakSelf = self;
    [self getTokenSuccess:^(NSDictionary *result) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.accessToken = result[@"access_token"];
        [strongSelf getTweetsWithQuery:query
                               maxId:nil
                               success:^(NSArray<TWTweetModel *> *models) {
                                   if (success) {
                                       success(models);
                                   }
                               } failure:^(NSError *error) {
                                   if (failure) {
                                       failure(error);
                                   }
                               }];
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (void)fetchMorePageDataWithQuery:(NSString *)query
                             maxId:(NSNumber *)ID
                           success:(CVDataRequestSuccessBlock)success
                           failure:(CVDataRequestFailureBlock)failure {
    [self getTweetsWithQuery:query
                       maxId:ID
                     success:^(NSArray<TWTweetModel *> *models) {
                         if (success) {
                             success(models);
                         }
                     } failure:^(NSError *error) {
                         if (failure) {
                             failure(error);
                         }
                     }];
}

- (void)getTweetsWithQuery:(NSString *)query
                     maxId:(NSNumber *)ID
                   success:(CVDataRequestSuccessBlock)success
                   failure:(CVDataRequestFailureBlock)failure  {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",self.accessToken]
                     forHTTPHeaderField:@"Authorization"];
    NSMutableDictionary *param = @{
                                   @"q" : query,
                                   @"count" : @(PAGE_COUNT),
                                   @"result_type" : @"recent",
                                   @"filter" : @"images",
                                   @"include_entities" : @YES,
                                   }.mutableCopy;
    if (ID) {
        param[@"max_id"] = ID;
    }
    [manager GET:[TWConfig searchTweetsUrl]
      parameters:param
        progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (![responseObject isKindOfClass:[NSDictionary class]]) {
                NSError *error = [NSError errorWithDomain:TW_HTTP_RESPONSE_ERROR_DOMAIN code:TW_HTTP_RESPONSE_ERROR_CODE_GENERAL userInfo:nil];
                if (failure) {
                    failure(error);
                }
                return;
            }
            NSArray *statuses = responseObject[@"statuses"];
            if (![statuses isKindOfClass:[NSArray class]]) {
                NSError *error = [NSError errorWithDomain:TW_HTTP_RESPONSE_ERROR_DOMAIN code:TW_HTTP_RESPONSE_ERROR_CODE_GENERAL userInfo:nil];
                if (failure) {
                    failure(error);
                }
                return;
            }
            NSMutableArray *res = [NSMutableArray array];
            for (NSDictionary *status in statuses) {
                TWTweetModel *model = [[TWTweetModel alloc] initWithDictionary:status];
                if (model) {
                    [res addObject:model];
                }
            }
            if (success) {
                success(res.copy);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(error);
            }
        }];
}

- (void)getTokenSuccess:(void(^)(NSDictionary *result))success
                failure:(void(^)(NSError *error))failure {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *key = [NSString stringWithFormat:@"Basic %@", [TWStringUtil base64:[NSString stringWithFormat:@"%@:%@",[TWConfig consumerKey], [TWConfig consumerSecret]]]];
    [manager.requestSerializer setValue:key forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=UTF-8"
                     forHTTPHeaderField:@"Content-Type"];
    [manager POST:[TWConfig tokeUrl]
       parameters:@{
                    @"grant_type" : @"client_credentials",
                    }
        progress:^(NSProgress * _Nonnull uploadProgress) {
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (![responseObject isKindOfClass:[NSDictionary class]]) {
                NSError *error = [NSError errorWithDomain:TW_HTTP_RESPONSE_ERROR_DOMAIN code:TW_HTTP_RESPONSE_ERROR_CODE_GENERAL userInfo:nil];
                if (failure) {
                    failure(error);
                }
                return;
            }
            if (success) {
                success(responseObject);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (failure) {
                failure(error);
            }
        }];
}

@end
