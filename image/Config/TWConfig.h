//
//  TWConfig.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...)
#endif

extern NSString * const TW_HTTP_RESPONSE_ERROR_DOMAIN;

typedef NS_ENUM(NSUInteger, TW_HTTP_RESPONSE_ERROR_CODE) {
    TW_HTTP_RESPONSE_ERROR_CODE_GENERAL = 1001,
};

@interface TWConfig : NSObject

+ (NSInteger)screenHeight;
+ (NSInteger)screenWidth;

+ (NSString *)consumerKey;
+ (NSString *)consumerSecret;

+ (NSString *)tokeUrl;
+ (NSString *)searchTweetsUrl;

@end
