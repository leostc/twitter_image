//
//  TWGridImageViewController.m
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import "TWGridImageViewController.h"
#import "TWGridImageCollectionViewCell.h"
#import "TWGridImageViewService.h"
#import "TWTweetModel.h"
#import "TWFullScreenView.h"
#import "TWConfig.h"
#import "MJRefresh.h"

static NSString *const INITIAL_SEARCH_CONTENT = @"#lovewhereyouwork";
static NSString *const GRID_IMAGE_CELL_IDENTIFIER = @"TWGridImageCollectionViewCell";
static NSInteger const NUMBER_OF_COLUMNS = 2;
static NSInteger const CELL_SPACE = 10;

@interface TWGridImageViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) TWGridImageViewService *gridImageViewService;
@property (strong, nonatomic) NSMutableArray <TWTweetModel *> *models;
@property (copy, nonatomic) NSString *query;

@end

@implementation TWGridImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.query = INITIAL_SEARCH_CONTENT;
    [self setupCollectionView];
    [self setupNavBar];
}

- (void)setupNavBar {
    self.navigationItem.title = self.query;
    UIButton *rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightNavButton setTitle:@"Search" forState:UIControlStateNormal];
    [rightNavButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [rightNavButton addTarget:self action:@selector(searchButtonClicked) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightNavButton];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)setupCollectionView {
    [self.collectionView registerNib:[UINib nibWithNibName:GRID_IMAGE_CELL_IDENTIFIER bundle:nil] forCellWithReuseIdentifier:GRID_IMAGE_CELL_IDENTIFIER];
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    
    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    // Begin to reload
    [self.collectionView.mj_header beginRefreshing];
}

- (void)loadNewData {
    __weak __typeof(self) weakSelf = self;
    [self.gridImageViewService fetchFistPageDataWithQuery:self.query
                                                  success:^(NSArray<TWTweetModel *> *models) {
                                                      __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                      strongSelf.models = models.mutableCopy;
                                                      [strongSelf.collectionView reloadData];
                                                      [strongSelf.collectionView.mj_header endRefreshing];
                                                  } failure:^(NSError *error) {
                                                      __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                      [strongSelf.collectionView.mj_header endRefreshing];
                                                  }];
}

- (void)loadMoreData {
    __weak __typeof(self) weakSelf = self;
    NSNumber *maxId = @(self.models.lastObject.ID.integerValue - 1);
    [self.gridImageViewService fetchMorePageDataWithQuery:self.query
                                                    maxId:maxId
                                                  success:^(NSArray<TWTweetModel *> *models) {
                                                      __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                      [strongSelf.models addObjectsFromArray:models];
                                                      [strongSelf.collectionView reloadData];
                                                      [strongSelf.collectionView.mj_footer endRefreshing];
                                                  } failure:^(NSError *error) {
                                                      __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                      [strongSelf.collectionView.mj_footer endRefreshing];
                                                  }];
}

- (void)searchButtonClicked {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Search Twitter";
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.query = [[[alertController textFields] firstObject] text];
        self.navigationItem.title = self.query;
        [self.collectionView.mj_header beginRefreshing];
    }];
    [alertController addAction:confirmAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.models.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TWGridImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:GRID_IMAGE_CELL_IDENTIFIER forIndexPath:indexPath];
    NSInteger row = indexPath.row;
    [cell loadViewWithModel:self.models[row]];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    TWFullScreenView *fullScreeView = [[TWFullScreenView alloc] initWithFrame:self.navigationController.view.bounds
                                                                        model:self.models[row]];
    [self.navigationController.view addSubview:fullScreeView];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (self.view.frame.size.width - CELL_SPACE * (NUMBER_OF_COLUMNS + 1)) / NUMBER_OF_COLUMNS;
    return CGSizeMake(width, width);
}

#pragma mark - getter

- (TWGridImageViewService *)gridImageViewService {
    if (!_gridImageViewService) {
        _gridImageViewService = [[TWGridImageViewService alloc] init];
    }
    return _gridImageViewService;
}

@end

