//
//  TWStringUtil.h
//  image
//
//  Created by HaoLi on 29/03/2018.
//  Copyright © 2018 twitter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TWStringUtil : NSObject

+ (NSString *)base64:(NSString *)string;

@end
